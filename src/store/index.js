import { createStore } from 'vuex'
import { v4 as uuidv4 } from "uuid";

export default createStore({
  state: {
    books: [],
    update: false,
  },
  mutations: {
    addBook(state,payload){
      state.books.push(payload)
      
    },
    deleteBook(state,payload){
      state.books = state.books.filter(book => book.id != payload.id)
    },
    updateBook(state,payload){
      let b = state.books.findIndex(book => book.id == payload.id)
      state.books.splice(b, 1, payload)
    }
  },
  actions: {
    addBook({commit},book){
      commit('addBook',{
        title: book.value,
        id: uuidv4(),
      })
      book.value = "";
    },

    deleteBook({commit},book){
      commit('deleteBook',book)
    },

    updateBook({commit},book){
      commit('updateBook',{
        title: book.value,
        id: tmpBook.value.id,
      })
      update.value = false;
      book.value = "";
    }
  },
  getters:{
    getAllBooks(state){
      return state.books
    },

    getUpdate(state){
      return state.update = !state.update
    }
  },
  modules: {

  }
})
